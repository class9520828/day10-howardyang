package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;
    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return EmployeeMapper.toResponseList(jpaEmployeeRepository.findAll());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new));
    }

    public void update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return EmployeeMapper.toResponseList(jpaEmployeeRepository.findByGender(gender));
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee savedEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(savedEmployee);
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {

        return EmployeeMapper.toResponseList(jpaEmployeeRepository
                .findAll(PageRequest.of(page-1, size))
                .getContent());

    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
