O(Objective):
+ In the morning, I consolidated and reviewed yesterday's knowledge through Code Review.
+ I learned how to use Mapper to specify the input and output in Spring Boot, and conducted corresponding exercises.
+ Through the teacher's explanation in class, I have gained a deeper understanding of the annotations used in Spring Boot and gradually gained a clear understanding of their respective roles.
+ I have learned and understood the basic operations of Flyway, which is mainly used to upgrade the database structure and data inside the application version while constantly upgrading.

R(Reflective):
+ Fruitful

I(Interpretive):
+ After afternoon practice, I am proficient in using Mapper to construct requests and responses.
+ I am not very clear about some other operations of Flyway and its role in actual project development, which requires further learning.

D(Decision):
+ Continue to review the relevant content of Spring Boot learned this week.
+ Continue to learn the construction method of query statements in JPARrepository.
